# 抖音同款动画相册
[![forthebadge](https://forthebadge.com/images/badges/uses-html.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/uses-css.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)

## 展示
![动态展示](./example/gifshow.gif)
## 使用方法：
1. 选择6张分辨率一致的图片放到img文件夹
2. 在index.html中修改img url:
例如：
```
 .minbox li:nth-child(1){
     transform: translateZ(100px);
     background-image: url('./img/1.jpeg');
     background-size: cover;
}

```
url('./img/1.jpeg') 中 1.jpeg 后缀替换成img文件名字后缀
## 问题：
欢迎在issue中提出你的想法。
## Enjoy ！
